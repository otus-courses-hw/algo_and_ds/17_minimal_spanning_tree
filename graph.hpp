#include <set>
#include <utility>
#include <vector>

class graph
{
    private:
        struct edge
        {
            unsigned first_v;
            unsigned second_v;
            double weight;

            edge() = delete;
            ~edge() = default;
            edge(unsigned, unsigned, double);
            
            bool operator<(const edge&) const;
        };

        struct union_find
        {
            std::vector<unsigned> parents;

            union_find() = default;
            ~union_find() = default;
            union_find(unsigned);
            union_find(union_find&&) noexcept;
            union_find& operator=(union_find&&) noexcept;
        };

        unsigned m_max_vertex;
        union_find m_union;
        std::multiset<edge> m_edges;
        using minimal_spanning_tree = std::vector<std::pair<unsigned, unsigned>>;

        void make_union_find();
        unsigned parent(unsigned);
        void merge(unsigned, unsigned);

    public:
        graph();
        ~graph() = default;

        void add_edge(unsigned, unsigned, double);
        minimal_spanning_tree kruskal();
};
