#include <catch2/catch.hpp>
#include "graph.hpp"

TEST_CASE("happy pass kruskal")
{
    graph gr;
    gr.add_edge(0, 1, 4.);
    gr.add_edge(1, 2, 2.);
    gr.add_edge(2, 3, 3.);
    gr.add_edge(3, 4, 4.);
    gr.add_edge(4, 5, 4.);
    gr.add_edge(5, 6, 3.);
    gr.add_edge(0, 5, 3.);
    gr.add_edge(1, 4, 1.);
    gr.add_edge(1, 7, 3.);
    gr.add_edge(2, 6, 2.);
    gr.add_edge(4, 7, 2.);
    /*          GRAPH
     *
     *     0---(4)--1---(2)--2----
     *     |        | \      |   |
     *     |        | (3)    |   |
     *    (3)      (1) 7    (3)  |
     *     |        | (2)    |   |
     *     |        | /      |   |
     *     5---(4)--4---(4)--3   |
     *      \                    |
     *      (3)                  |
     *        \                  |
     *         6---------------(2)
     */
    std::vector<std::pair<unsigned, unsigned>> minimal_spanning_tree {
            {1,4},
            {1,2},
            {2,6},
            {4,7},
            {2,3},
            {5,6},
            {0,5}
    };
    /*          MINIMAL SPANNING TREE
     *
     *     0        1---(2)--2----
     *     |        |        |   |
     *     |        |        |   |
     *    (3)      (1) 7    (3)  |
     *     |        | (2)    |   |
     *     |        | /      |   |
     *     5        4        3   |
     *      \                    |
     *      (3)                  |
     *        \                  |
     *         6---------------(2)
     */
    CHECK(minimal_spanning_tree == gr.kruskal());
}
