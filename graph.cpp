#include "graph.hpp"
#include <algorithm>
#include <utility>
#include <vector>

graph::edge::edge(unsigned a, unsigned b, double w) :
    first_v(a),
    second_v(b),
    weight(w)
{
}

bool graph::edge::operator<(const edge &other) const
{
    return weight < other.weight;
}

graph::union_find::union_find(unsigned size) :
    parents(size)
{
}

graph::union_find::union_find(union_find &&other) noexcept:
    parents(std::move(other.parents))
{
}

auto graph::union_find::operator=(union_find &&other) noexcept -> union_find&
{
    parents = std::move(other.parents);
    return *this;
}

graph::graph() :
    m_max_vertex(0)
{
}

void graph::add_edge(unsigned a, unsigned b, double w)
{
    if (auto greatest = std::max(a, b); greatest > m_max_vertex)
        m_max_vertex = greatest; // for help to create union_set

    m_edges.emplace(a, b, w); // sort edges by weight (order by asc)
}

void graph::make_union_find()
{
    m_union = union_find(m_max_vertex + 1);

    for (const auto &e : m_edges)
    {
        m_union.parents.at(e.first_v) = e.first_v;
        m_union.parents.at(e.second_v) = e.second_v;
    }
}

unsigned graph::parent(unsigned vertex)
{
    if (m_union.parents.at(vertex) == vertex)
        return vertex;

    auto pr = parent(m_union.parents.at(vertex));

    m_union.parents.at(vertex)  = pr;

    return pr;
}

void graph::merge(unsigned a, unsigned b)
{
    auto parent_a = parent(a);
    auto parent_b = parent(b);

    if (parent_a != parent_b)
        m_union.parents.at(parent_a) = parent_b;
}

auto graph::kruskal() -> minimal_spanning_tree
{
    make_union_find();

    minimal_spanning_tree result;

    auto push = [&result, this] (const edge &e) {
        result.emplace_back(e.first_v, e.second_v);
        merge(e.first_v, e.second_v);
    };

    for (const auto &e : m_edges)
    {
        if (result.size() == m_union.parents.size() - 1) // MINIMAL_NUMBER_OF_EDGES = VERTICES - 1
            break; // get minimal spanning tree

        auto p_a = parent(e.first_v);
        auto p_b = parent(e.second_v);

        if (p_a == p_b) // BOTH VERTICES FROM THE SAME COMPONENT
            continue;
        else
            push(e);
    }

    return result;
}
